'use strict'

const Hapi = require('hapi')

const config = require('config')

const {numberToLcd} = require('../app/number-to-lcd')

const server = new Hapi.Server()

const port = config.get('server.port')
const listen = config.get('server.listen')

server.connection({port: port, host: listen})

server.route({
    method: 'GET',
    path: '/',
    handler: function(request, reply) {
        const exampleUrl = `//${request.headers.host}/lcd_numbers/12345`
        reply(`Please use the "/lcd_numbers/{number}" route.<br/>Example: <a href="${exampleUrl}">${exampleUrl}</a>`)
    }
})

server.route({
    method: 'GET',
    path: '/healthz',
    handler: function(request, reply) {
        reply(`healthy`)
    }
})

server.route({
    method: 'GET',
    path: '/lcd_numbers/{number}',
    handler: function(request, reply) {
        const numberAsString = encodeURIComponent(request.params.number)
        const number = parseInt(numberAsString)
        reply(numberToLcd(number))
            .header('Content-Type', 'text/plain; charset=utf-8')
    }
})

server.start((err) => {
    if (err) {
        throw err
    }
    console.log(`Server running at: ${server.info.uri}`)
})
