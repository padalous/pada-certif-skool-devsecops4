# Examen Programme DevSecOps 

## 1- Description

L'examen est basé sur un exercice pratique constitué d'un environnement monté à l'aide d'infrastructure as Code.
Il y a beaucoup de choses à redire sur ce code et la plateforme qui en découle. Votre travail se découpe de la façon suivante : 
- Analyse de la situation (code de l'infrastucture)
- QCM, permettant de guider dans l'analyse, ci-dessous. Merci de remplire les bonnes réponses dans un fichier "stuff/REPONSES.yml" 
- Proposition d'amélioration sous forme d'une restitution rapide (via GChat aux examinateurs)
- Premières corrections des points qui vous semblent les plus importants, suivant vos critères (testabilité, architecture, sécurité, service rendu...)

La fin de l'examen se réalise par un commit dans un dépôt de code (que vous créerez sur le gitlab OCTO pour l'occasion), dans lequel des changements sont attendus : les premières modifications que vous proposez de réaliser après la petite restitution d'analyse

> **NOTA** : votre commit doit être pushé dans un repo sur lequel vous donnerez les droits aux examinateurs et uniquement sur le temps imparti !
>
> **NOTA** : Pas de commit, pas de chocolat
>
> **NOTA** : Pensez à faire une branche séparée tous vos commits sur votre branche !!

## 2- Prendre en main son environnement

Un environnement a été créé pour chaque candidat : 
- une machine de dev, avec :
  - Terraform, Ansible, le client `kubectl`, le tout déjà configuré
  - un dépôt de code Terraform et Ansible avec un commit initial => c'est ici que vous allez devoir faire un commit
  - le code Terraform a déjà été lancé et a créé tout un environnement
  - le code Ansible a déjà été lancé au travers du playbook `install.yml`
- une architecture complète a été généré gràce à la machine de dev :
  - VPC, Subnet, SG
  - Instances EC2
  - ALB
- une machine Kubernetes partagée par tous les candidats a été créée et la configuration a déjà été effectuée pour s'y connecter (kubeconfig)

Certaines questions du QCM vont vous demander de lancer **Terraform**. Pour ce faire, vous devez au préalable : 
- aller dans le répertoire `~/pada/stuff/terraform`

Certaines questions du QCM vont vous demander de lancer **Ansible**. Pour ce faire, vous devez au préalable : 
- aller dans le répertoire `~/pada/stuff/ansible`
- sourcer le virtualenv `. venv/bin/activate`

Si vous souhaitez aller sur les machines, il est possible de déterminer leur noms et IP avec la commande suivante :
`ansible-inventory --list`

Pour les plus aventureux⋅ses des choses comme `ansible-inventory --list | jq '._meta.hostvars[] | {name: .tags.Name, ip: .public_ip_address}'` pourraient même s'avérer utiles, surtout si vous souhaitez vous connecter aux machines (avec l'utilisateur `ubuntu` et le port `20022` soit dit en passant).

Certaines questions du QCM vont vous demander de regarder comment l'application est construite avec **Docker**. Pour ce faire, vous devez au préalable : 
- aller dans le répertoire `~/pada/stuff/app-lcd`
- regarder le code source (bienvenue dans le monde merveilleux de Node.js) et le `Dockerfile`

Certaines questions du QCM vont vous demander de travailler avec un cluster **Kubernetes**. Pour ce faire : 
- tout l'environnement est déjà configuré : les commandes à base de `kubectl` sont déjà fonctionnelles et limitées pour ne pas faire trop de bêtises sur les environnemements des autres candidats. `kubeon` et l'auto-completion sont en principe fonctionnels.
- ce qui a été déployé pour vous a été fait sur la base des manifestes dans le répertoire `~/pada/stuff/kubernetes`

## 3- QCM

> **Question 1 (Terraform)**
>
> Comment interpréter le security group `devsecops-vpc-pada` en lisant le code Terraform ?
>
> - a) tous les flux sont ouverts
> - b) tous les flux TCP sont ouverts
> - c) certains flux sont ouverts en entrée, tous les flux sont ouverts en sortie
> - d) aucunes des réponses précédentes

> **Question 2 (Ansible)**
>
> dans le dossier stuff/ansible, lancer la commande suivante : 
> `ansible-playbook install.yml`
> Que constatons-nous ?
>
> - a) l’exécution se termine sans erreur
> - b) il y a des changements à chaque exécution
> - c) l’inventaire est statique
> - d) les réponses a) et b)

> **Question 3 (Infra as Code)**
>
> Par rapport à la question précédente, par quels changements voudriez-vous commencer ?
>
> - a) ajouter une tâche Ansible
> - b) ajouter un test Molecule ou le lancer s'il existe déjà
> - c) poser des tags sur certaines tâches Ansible
> - d) rien, ça marche bien comme cela

> **Question 4 (Terraform + Ansible)**
>
> En lisant le code Terraform et Ansible, quels sont les composants qui répondent aux requêtes sur http://prometheus.pada.aws.octo.training ?
>
> - a) prometheus, via un HAProxy sur m1 et m2, via le LoadBalancer
> - b) prometheus, via un HAProxy sur m1, via le loadBalancer
> - c) prometheus, via un NGinx sur m1, via le loadbalancer
> - d) prometheus, via un HAProxy sur m1 et m2, via un round robin DNS route53

> **Question 5 (Prometheus)**
>
> Quels sont les éléments monitorés par le Prometheus déployé à l'URL http://prometheus.pada.aws.octo.training ?
>
> - a) 4 éléments : 3 VM au travers du node_exporter et Prometheus qui se monitore lui-même
> - b) 2 éléments : 1 node_exporter et Prometheus qui se monitore lui-même
> - c) 2 éléments : les deux VM `m1` et `m2`
> - d) 0 élément : Rien n'est remonté dans Prometheus

> **Question 6 (Système)**
>
> Quelle est la version du kernel Linux sur la machine `m1` ?
>
> **Note** : vous avez sans doute plusieurs moyens de trouver cette information...
>
> - a) 3.18.6-aws
> - b) 5.4.0-1035-aws
> - c) 4.15.0-1063-aws
> - d) 5.5.10-arch1-1

> **Question 7 (Application, Dockerfile)**
>
> Quelle version d'`eslint` est présente dans l'image finale construite ?
>
> - a) 3.19.0
> - b) eslint n'est pas installé dans l'image finale
> - c) 3.0.1
> - d) 1.0.0

> **Question 8 (Application, Dockerfile, CI)**
>
> En lançant dans `~/pada/stuff/app-lcd` la commande `docker build -t lcd-app .` et en regardant le Dockerfile laquelle de ces affirmations est fausse ?
>
> - a) En tout, 7 images Docker devraient être théoriquement produites, mais l'étape de scan de vulnérabilité trouve des failles critiques dans les dépendances de l'application, ce qui bloque la construction de l'image finale
> - b) L'image finale est plus petite que les images utilisées pour les différents tests
> - c) Deux types de tests sont exécutés lors de la construction de l'image : des tests unitaires et des tests end-to-end (e2e)
> - d) L'application écoute sur le port 3000 au démarrage

> **Question 9 (Kubernetes)**
>
> Un déploiement applicatif a été effectué dans K8s, dans quel namespace il a été effectué ?
>
> - a) kube-system
> - b) kube-public
> - c) default
> - d) pada

> **Question 10 (Kubernetes)**
>
> Pourquoi l'URL http://app.k8s.pada.aws.octo.training ne fonctionne pas correctement dans Kubernetes ?
>
>  - a) il n'y a pas d'ingress avec ce nom
>  - b) l'ingress existe, pointe correctement sur un service, mais pas sur le bon port
>  - c) les pods pointés par l'ingress sont redémarrés en boucle à cause d'une mauvaise image Docker
>  - d) les pods pointés par l'ingress sont redémarrés en boucle à cause d'une mauvaise probe

